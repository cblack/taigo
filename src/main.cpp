#include <QtCore>

#ifdef Q_OS_LINUX

#include <QApplication>
#define AppType QApplication

#else

#include <QGuiApplication>
#define AppType QGuiApplication

#endif

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "Taigo.h"

int main(int argc, char *argv[])
{
    AppType app(argc, argv);
    app.setOrganizationName("KDE");
    app.setOrganizationDomain("kde.org");
    app.setApplicationName("Taigo");

    qmlRegisterType<Taigo>("Taigo", 1, 0, "Taigo");


    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("TaigoDebug", !qgetenv("TAIGO_DEBUG").isEmpty());
    const QUrl url(QStringLiteral("qrc:/Main.qml"));
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreated,
        &app,
        [url](QObject *obj, const QUrl &objUrl) {
            if ((obj == nullptr) && url == objUrl) {
                QCoreApplication::exit(-1);
            }
        },
        Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
