import QtQuick 2.10
import Taigo 1.0
import QtScxml 5.10
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.10 as QQC2
import org.kde.kirigami 2.12 as Kirigami

Kirigami.AbstractCard {
    readonly property var mu: {
        return {
            Taigochi
        }
    }
    anchors {
        top: parent.top
        topMargin: Kirigami.Units.smallSpacing
        left: parent.left
        leftMargin: Kirigami.Units.smallSpacing
        right: parent.right
        rightMargin: Kirigami.Units.smallSpacing
    }
    contentItem: ColumnLayout {
        Kirigami.Heading {
            text: taigochi.tData[taigochi.kind].name
            horizontalAlignment: Text.AlignHCenter

            Layout.fillWidth: true
        }
        RowLayout {
            Image {
                Layout.preferredHeight: implicitHeight / 2
                Layout.preferredWidth: implicitWidth / 2

                source: `qrc:/images/taigochi/${taigochi.tData[taigochi.kind].name}/normal`
            }
            Kirigami.Separator {
                Layout.fillHeight: true
            }
            ColumnLayout {
                spacing: 2

                Layout.leftMargin: 4

                QQC2.Label {
                    text: taigochi.tGenderNames[taigochi.tData[taigochi.kind].gender]
                }
                QQC2.Label {
                    text: `${taigochi.tData[taigochi.kind].baseWeight + taigochi.tgData.weight}g`
                }
                QQC2.Label {
                    text: `${taigochi.tgData.age}yr`
                }
            }

            Layout.alignment: Qt.AlignHCenter
        }
        Kirigami.Separator {
            Layout.fillWidth: true
        }
        Kirigami.FormLayout {
            component Bar : RowLayout {
                id: _bar
                required property real against

                Repeater {
                    model: 4
                    Rectangle {
                        color: _bar.against > modelData ? Kirigami.Theme.highlightColor : "transparent"
                        border.color: Kirigami.Theme.textColor
                        border.width: 1

                        implicitHeight: 20
                        implicitWidth: 20
                        radius: width / 2
                    }
                }
            }
            Bar { against: taigochi.tgData.hunger; Kirigami.FormData.label: "Food:" }
            Bar { against: taigochi.tgData.happy; Kirigami.FormData.label: "Happiness:" }
            Layout.fillWidth: true
        }
    }
}