import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.10 as QQC2
import org.kde.kirigami 2.10 as Kirigami
import Qt.labs.settings 1.0

Window {
    minimumHeight: col.implicitHeight
    height: col.implicitHeight
    maximumHeight: col.implicitHeight

    minimumWidth: col.implicitWidth
    width: col.implicitWidth
    maximumWidth: col.implicitWidth

    title: "Gametown"

    component TownTile : Image {
        required property int number

        source: `qrc:/images/bgs/town-tile-${number}`
    }

    onClosing: taigochi.stateMachine.submitEvent("ReturnHome")

    QQC2.Control {
        id: col

        background: RowLayout {
            spacing: 0

            TownTile { number: 0 }
            TownTile { number: 1 }
            TownTile { number: 2 }
            TownTile { number: 3 }
        }
        contentItem: Item {
            Kirigami.AbstractCard {
                anchors.centerIn: parent

                padding: Kirigami.Units.gridUnit
                leftPadding: Kirigami.Units.gridUnit
                rightPadding: Kirigami.Units.gridUnit
                topPadding: Kirigami.Units.gridUnit
                bottomPadding: Kirigami.Units.gridUnit

                contentItem: ColumnLayout {
                    spacing: 0

                    Kirigami.Heading {
                        text: "Rock Paper Scissors"
                    }
                    Image {
                        source: `qrc:/images/animations/rps/scissor`

                        Layout.alignment: Qt.AlignHCenter
                    }
                    QQC2.Button {
                        text: "Play"

                        onClicked: mu.show()

                        RockPaperScissors { id: mu }

                        Layout.fillWidth: true
                    }
                }
            }
        }
    }
}