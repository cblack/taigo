import QtQuick 2.10
import Taigo 1.0
import QtScxml 5.10
import QtQuick.Layouts 1.10
import org.kde.kirigami 2.12 as Kirigami

Image {
    id: taigochiRoot

    required property int kind
    required property string taigochiState

    property alias tgData: __private
    property alias stateMachine: taigoState

    Taigo {
        id: taigoState

        running: true
    }
    EventConnection {
        stateMachine: taigoState
        events: ["FuckingDie"]
        onOccurred: {
            taigochiRoot.kind = Taigochi.Species.Molichi
            __kvetchColumn.visible = false
            appRoot.blackout()
            appRoot.bgImage = "graveyard"
            trans.x = 0
        }
    }
    EventConnection {
        stateMachine: taigoState
        events: ["GoToDaycare"]
        onOccurred: {
            taigochiRoot.taigochiState = "zzz"
            appRoot.bgImage = "daycare"
            appRoot.dim = true
            trans.x = 0
            __kvetchColumn.visible = false
        }
    }
    EventConnection {
        stateMachine: taigoState
        events: ["GoToGaming"]
        onOccurred: {
            taigochiRoot.visible = false
            appRoot.bgImage = "away"
            __kvetchColumn.visible = false
        }
    }
    EventConnection {
        stateMachine: taigoState
        events: ["ReturnHome"]
        onOccurred: {
            taigochiRoot.visible = true
            taigochiRoot.taigochiState = "normal"
            appRoot.bgImage = "home"
            appRoot.dim = false
            __kvetchColumn.visible = true
        }
    }
    EventConnection {
        stateMachine: taigoState
        events: ["Hatch"]
        onOccurred: {
            __private.stage = "baby"
        }
    }
    EventConnection {
        stateMachine: taigoState
        events: ["Hatch", "InitAlive"]
        onOccurred: {
            taigochiRoot.visible = true
            taigochiRoot.taigochiState = "normal"
            appRoot.bgImage = "home"
            appRoot.dim = false
            __kvetchColumn.visible = true
        }
    }
    EventConnection {
        stateMachine: taigoState
        events: ["InitEgg"]
        onOccurred: {
            taigochiRoot.visible = false
            taigochiRoot.taigochiState = "normal"
            appRoot.bgImage = "sleepingsun"
            appRoot.dim = true
            __kvetchColumn.visible = false
        }
    }

    enum Complaints {
        Hunger,
        Bored,
        Fat
    }
    enum Species {
        // Death
        Molichi,
        // Baby
        Taikocha,
        Taikoche,
        Taikochi
    }
    enum Genders {
        Lunarian, // fem
        Solarian, // masc
        Stellarian, // 3rd gender
        Dead // 4th gender
    }

    readonly property var tGenderNames: {
        const thing = {}
        thing[Taigochi.Genders.Lunarian] = "Lunarian"
        thing[Taigochi.Genders.Solarian] = "Solarian"
        thing[Taigochi.Genders.Stellarian] = "Stellarian"
        thing[Taigochi.Genders.Dead] = "Dead"
        return thing
    }
    readonly property var tData: {
        const thing = {}

        thing[Taigochi.Species.Molichi] = {
            name: "Molichi",
            gender: Taigochi.Genders.Dead,
            baseWeight: 1,
        }

        thing[Taigochi.Species.Taikocha] = {
            name: "Taikocha",
            gender: Taigochi.Genders.Lunarian,
            baseWeight: 1,
        }
        thing[Taigochi.Species.Taikoche] = {
            name: "Taikoche",
            gender: Taigochi.Genders.Stellarian,
            baseWeight: 1,
        }
        thing[Taigochi.Species.Taikochi] = {
            name: "Taikochi",
            gender: Taigochi.Genders.Solarian,
            baseWeight: 1,
        }

        return thing
    }

    TaigochiState {
        id: __private
    }

    source: `qrc:/images/taigochi/${tData[kind].name}/${taigochiState}`

    transform: Translate {
        id: trans

        x: 20
        Behavior on x {
            NumberAnimation {
                duration: 100
                easing.type: Easing.InOutCirc
            }
        }

        property Animation ani: SequentialAnimation {
            id: ani

            running: taigochiRoot.state == "home"
            loops: Animation.Infinite

            property int offset: 0
            onOffsetChanged: {
                if (offset < -5) {
                    offset = -5
                } else if (offset > 5) {
                    offset = 5
                }
            }

            ScriptAction {
                function randInt(min, max) {
                    return Math.floor(Math.random() * (max - min + 1) ) + min
                }
                script: {
                    const num = randInt(-2, 3)
                    ani.offset += num
                    trans.x = ani.offset * 20
                }
            }
            PauseAnimation {
                duration: 1000
            }
        }
    }

    KvetchColumn {
        id: __kvetchColumn
    }

    SequentialAnimation {
        running: taigochiRoot.state == "home"
        loops: Animation.Infinite

        PauseAnimation {
            duration: 3273
        }
        ScriptAction {
            script: {
                taigochiRoot.taigochiState = "blink"
            }
        }
        PauseAnimation {
            duration: 100
        }
        ScriptAction {
            script: {
                taigochiRoot.taigochiState = "normal"
            }
        }
    }

    states: [
        State {
            name: "gaming"
            when: taigoState.Gaming
        },
        State {
            name: "sleeping"
            when: taigoState.Daycare
        },
        State {
            name: "home"
            when: taigoState.Home
        }
    ]
}
