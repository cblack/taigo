import QtQuick 2.10
import Taigo 1.0
import QtScxml 5.10
import QtQuick.Layouts 1.10
import org.kde.kirigami 2.12 as Kirigami

Image {
    id: __image

    property string frame: "normal"
    source: `qrc:/images/taigochi/Egg/${frame}.svg`

    property bool cracking: false
    property bool shaking: false

    Timer {
        interval: 60 * appRoot.speedFactor
        running: true
        onTriggered: __image.cracking = true
    }

    transform: Translate {
        id: trans
    }

    SequentialAnimation {
        running: !cracking
        loops: Animation.Infinite
        PropertyAction { target: __image; property: "frame"; value: "normal" }
        PauseAnimation { duration: 150 }
        PropertyAction { target: __image; property: "frame"; value: "sqoosh" }
        PauseAnimation { duration: 200 }
    }

    SequentialAnimation {
        running: shaking
        loops: Animation.Infinite
        PropertyAction { target: trans; property: "x"; value: 5 }
        PauseAnimation { duration: 150 }
        PropertyAction { target: trans; property: "x"; value: -5 }
        PauseAnimation { duration: 150 }
    }

    SequentialAnimation {
        running: cracking
        PauseAnimation { duration: 2000 }
        ScriptAction { script: __image.shaking = true }
        PauseAnimation { duration: 1000 }
        PropertyAction { target: __image; property: "frame"; value: "crack-1" }
        PauseAnimation { duration: 200 }
        PropertyAction { target: __image; property: "frame"; value: "crack-2" }
        PauseAnimation { duration: 200 }
        PropertyAction { target: __image; property: "frame"; value: "crack-3" }
        PauseAnimation { duration: 200 }
        PropertyAction { target: __image; property: "frame"; value: "crack-4" }
        PauseAnimation { duration: 500 }
        ScriptAction {
            script: {
                appRoot.blackout()
                taigochi.stateMachine.submitEvent("Hatch")
            }
        }
    }
}