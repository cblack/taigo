import QtQuick 2.10

Obj {
    id: __private

    readonly property var toObject: {
        return {
            "hunger": this.hunger,
            "happy": this.happy,
            "age": this.age,
            "weight": this.weight,
            "missedCalls": this.missedCalls,
            "careMisses": this.careMisses,
            "stage": this.stage
        }
    }
    function fromObject(obj) {
        if (obj.hunger != undefined)
            this.hunger = obj.hunger
        if (obj.happy != undefined)
            this.happy = obj.happy
        if (obj.age != undefined)
            this.age = obj.age
        if (obj.weight != undefined)
            this.weight = obj.weight
        if (obj.missedCalls != undefined)
            this.missedCalls = obj.missedCalls
        if (obj.careMisses != undefined)
            this.careMisses = obj.careMisses
        if (obj.stage != undefined)
            this.stage = obj.stage
    }

    function randFloat(min, max) {
        return (Math.random() * (max - min + 1)) + min
    }
    function randInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min
    }

    component NormalTimer : Timer {
        running: taigoState.Alive && taigoState.Home
        repeat: true
    }

    // Life stage.
    // stage = "egg" | "baby"
    property string stage: "egg"

    // Needs
    property real hunger: 2
    function feed() {
        if (this.hunger == 4) {
            this.weight++
        }
        this.hunger = Math.min(4, this.hunger + 1)
    }
    function treat() {
        this.weight++
        this.hunger = Math.min(4, this.hunger + 1)
        this.happy = Math.min(4, this.happy + 1)
    }
    function tickHunger() {
        if (randInt(0, 2) == 1) {
            hunger = Math.max(0, hunger - randFloat(0.1, 0.5))
        }
    }
    NormalTimer {
        interval: 120 * appRoot.speedFactor
        onTriggered: __private.tickHunger()
    }
    property real happy: 2
    function tickHappy() {
        if (randInt(0, 2) == 1) {
            happy = Math.max(0, hunger - randFloat(0.1, 0.5))
        }
    }
    function play() {
        this.weight = Math.max(0, this.weight - 1)
        this.happy = Math.min(4, this.happy + 1)
    }
    NormalTimer {
        interval: 180 * appRoot.speedFactor
        onTriggered: __private.tickHappy()
    }

    // About
    property int age: 0
    property real weight: 0
    function tickWeight() {
        if (randInt(0, 2) == 1) {
            weight = Math.max(0, weight - 1)
        }
    }
    NormalTimer {
        interval: 1800 * appRoot.speedFactor
        onTriggered: __private.tickHappy()
    }

    readonly property var kvetches: {
        let arr = []

        if (hunger <= 1)
            arr.push(Taigochi.Complaints.Hunger)
        if (happy <= 1)
            arr.push(Taigochi.Complaints.Bored)
        if (weight >= 4)
            arr.push(Taigochi.Complaints.Fat)

        return arr
    }

    property int missedCalls: 0
    property int careMisses: 0
    function evaluateDeath() {
        if (careMisses > 5) {
            taigoState.submitEvent("FuckingDie")
        }
    }
    function tickCare() {
        if (missedCalls >= 1 && kvetches.length > 0) {
            careMisses++
            missedCalls = 0
            evaluateDeath()
        } else if (missedCalls <= -1 && kvetches.length == 0) {
            this.missedCalls = 0
            this.careMisses--
        } else if (kvetches.length == 0) {
            this.missedCalls = -1
        } else {
            this.missedCalls = 1
        }
    }
    NormalTimer {
        interval: 300 * appRoot.speedFactor
        onTriggered: __private.tickCare()
    }
}