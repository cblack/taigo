import QtQuick 2.10
import Taigo 1.0
import QtScxml 5.10
import QtQuick.Layouts 1.10
import org.kde.kirigami 2.12 as Kirigami

ColumnLayout {
    anchors.bottom: parent.top
    anchors.bottomMargin: Kirigami.Units.largeSpacing
    anchors.horizontalCenter: parent.horizontalCenter

    component TextBubble : Kirigami.AbstractCard {
        id: _bub
        required property string cont

        Layout.maximumWidth: 200
        Layout.fillWidth: false

        contentItem: Kirigami.Heading {
            text: _bub.cont
            level: 4
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
        }
    }
    TextBubble {
        cont: "Play with me!"
        visible: __private.kvetches.includes(Taigochi.Complaints.Bored)

        Layout.alignment: Qt.AlignHCenter
    }
    TextBubble {
        cont: "I'm fat!"
        visible: __private.kvetches.includes(Taigochi.Complaints.Fat)

        Layout.alignment: Qt.AlignHCenter
    }
    TextBubble {
        cont: "Feed me!"
        visible: __private.kvetches.includes(Taigochi.Complaints.Hunger)

        Layout.alignment: Qt.AlignHCenter
    }
}