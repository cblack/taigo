import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.10 as QQC2
import org.kde.kirigami 2.10 as Kirigami
import Qt.labs.settings 1.0

Window {
    id: appRoot

    property real speedScale: 1.0
    property real speedFactor: speedScale * 1000.0
    property bool dim: false
    property string bgImage: "home"

    minimumHeight: col.implicitHeight
    height: col.implicitHeight
    maximumHeight: col.implicitHeight

    minimumWidth: col.implicitWidth
    width: col.implicitWidth
    maximumWidth: col.implicitWidth

    visible: true

    function blackout() {
        __blackoutAni.restart()
    }

    ColumnLayout {
        id: col
        spacing: 0

        QQC2.ToolBar {
            contentItem: RowLayout {
                id: statusBar
                property int idx: -1

                function set(v) {
                    if (this.idx == v) {
                        this.idx = -1
                    } else {
                        this.idx = v
                    }
                }

                Item { Layout.fillWidth: true }
                QQC2.Button {
                    icon.name: "love"
                    enabled: !taigochi.stateMachine.Egg
                    onClicked: statusBar.set(0)
                }
                QQC2.Button {
                    icon.name: "food"
                    enabled: taigochi.stateMachine.Home
                    onClicked: statusBar.set(1)
                }
                QQC2.Button {
                    icon.name: "input-gamepad"
                    enabled: (taigochi.stateMachine.Home || taigochi.stateMachine.Gaming)
                    onClicked: taigochi.stateMachine.Gaming ? taigochi.stateMachine.submitEvent("ReturnHome") : taigochi.stateMachine.submitEvent("GoToGaming")
                }
                QQC2.Button {
                    icon.name: "car"
                    enabled: (taigochi.stateMachine.Home || taigochi.stateMachine.Daycare)
                    onClicked: taigochi.stateMachine.Daycare ? taigochi.stateMachine.submitEvent("ReturnHome") : taigochi.stateMachine.submitEvent("GoToDaycare")
                }
                Item { Layout.fillWidth: true }
            }

            Layout.fillWidth: true
        }
        Image {
            source: `qrc:/images/bgs/${appRoot.bgImage}.svg`

            Egg {
                visible: taigochi.tgData.stage === "egg"

                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 40
            }
            Taigochi {
                id: taigochi

                kind: {
                    if (sets.data === "") {
                        return Math.floor(Math.random() * (4 - 1 + 1) ) + 1
                    } else {
                        let dat = JSON.parse(sets.data)

                        taigochi.tgData.fromObject(dat)

                        return dat.kind
                    }
                }
                taigochiState: "normal"

                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 40

                Component.onCompleted: {
                    if (taigochi.tgData.stage === "egg") {
                        taigochi.stateMachine.submitEvent("InitEgg")
                    } else {
                        taigochi.stateMachine.submitEvent("InitAlive")
                    }
                }
            }

            Rectangle {
                id: __blackout

                color: "black"
                anchors.fill: parent
                opacity: 0

                SequentialAnimation {
                    id: __blackoutAni

                    NumberAnimation {
                        target: __blackout
                        property: "opacity"
                        to: 1
                        duration: 0
                    }
                    PauseAnimation {
                        duration: Kirigami.Units.humanMoment
                    }
                    NumberAnimation {
                        target: __blackout
                        property: "opacity"
                        to: 0
                        duration: 3000
                    }
                }
            }

            Image {
                visible: appRoot.dim

                source: `qrc:/images/fgs/darkened.svg`

                anchors.fill: parent
            }
            StatusCard { visible: statusBar.idx === 0 }
            FoodCard { visible: statusBar.idx === 1 }
        }
    }

    Settings {
        id: sets

        property string data: ""
    }

    function save() {
        let obj = taigochi.tgData.toObject
        obj.kind = taigochi.kind

        sets.data = JSON.stringify(obj)
    }

    Component.onDestruction: save()

    DebugWindow {
        visible: TaigoDebug
    }
    GamingWindow {
        visible: taigochi.stateMachine.Gaming
    }

}
