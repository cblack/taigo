import QtQuick 2.10
import Taigo 1.0
import QtScxml 5.10
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.10 as QQC2
import org.kde.kirigami 2.12 as Kirigami

Kirigami.AbstractCard {
    anchors {
        top: parent.top
        topMargin: Kirigami.Units.smallSpacing
        left: parent.left
        leftMargin: Kirigami.Units.smallSpacing
        right: parent.right
        rightMargin: Kirigami.Units.smallSpacing
    }
    contentItem: ColumnLayout {
        Kirigami.Heading {
            text: "Feed your Taigochi"
            horizontalAlignment: Text.AlignHCenter

            Layout.fillWidth: true
        }
        RowLayout {
            spacing: 10

            component Fooder : Image {
                id: _fooder

                required property string kind
                property string state: "idle"
                readonly property bool aniRunning: _seqAni.running

                source: `qrc:/images/animations/${kind}/${state}`

                function go() {
                    _seqAni.restart()
                }

                SequentialAnimation {
                    id: _seqAni

                    PropertyAction { target: _fooder; property: "state"; value: "eat-1" }
                    PauseAnimation { duration: 600 }
                    PropertyAction { target: _fooder; property: "state"; value: "eat-2" }
                    PauseAnimation { duration: 600 }
                    PropertyAction { target: _fooder; property: "state"; value: "eat-3" }
                    PauseAnimation { duration: 600 }
                    PropertyAction { target: _fooder; property: "state"; value: "eat-4" }
                    PauseAnimation { duration: 600 }
                    PropertyAction { target: _fooder; property: "state"; value: "blank" }
                    PauseAnimation { duration: 600 }
                    PropertyAction { target: _fooder; property: "state"; value: "idle" }
                }

                Layout.alignment: Qt.AlignHCenter
            }

            ColumnLayout {
                Fooder {
                    id: mealer
                    kind: "meal"
                }
                QQC2.Button {
                    text: "Meal"
                    enabled: !mealer.aniRunning
                    onClicked: {
                        mealer.go()
                        taigochi.tgData.feed()
                    }
                    Layout.fillWidth: true
                }
            }

            ColumnLayout {
                Fooder {
                    id: treater
                    kind: "treat"
                }
                QQC2.Button {
                    text: "Treat"
                    enabled: !treater.aniRunning
                    onClicked: {
                        treater.go()
                        taigochi.tgData.treat()
                    }
                    Layout.fillWidth: true
                }
            }

            Layout.alignment: Qt.AlignHCenter
        }
    }
}