import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.10 as QQC2
import org.kde.kirigami 2.10 as Kirigami
import Qt.labs.settings 1.0

Kirigami.AbstractApplicationWindow {
    title: "Rock Paper Scissors"

    minimumHeight: col.implicitHeight
    height: col.implicitHeight
    maximumHeight: col.implicitHeight

    minimumWidth: col.implicitWidth
    width: col.implicitWidth
    maximumWidth: col.implicitWidth

    visible: false

    Kirigami.Theme.inherit: false
    Kirigami.Theme.colorSet: Kirigami.Theme.Window
    color: Kirigami.Theme.backgroundColor

    Obj {
        id: state

        readonly property var roles: ["paper", "rock", "scissor"]
        property int currentRoleIdx: 0
        readonly property string currentRole: roles[currentRoleIdx]

        property bool shuffling: !ani.running

        property string currentFace: "normal"

        Timer {
            interval: 100
            running: state.shuffling
            repeat: true
            onTriggered: {
                state.currentRoleIdx = (state.currentRoleIdx + 1) % 3
            }
        }

        function wins(idx) {
            // tie is considered happy
            if (idx === state.currentRoleIdx) {
                return true
            } else if ((idx+1)%3 === state.currentRoleIdx) { // player beating their pet not
                return false
            } else { // player losing is happy
                return true
            }
        }

        function play(idx) {
            ani.other = wins(idx) ? "yay" : "aww"
            if (wins(idx)) {
                taigochi.tgData.play()
            }
            ani.restart()
        }

        SequentialAnimation {
            id: ani
            property string other

            PropertyAction { target: state; property: "currentFace"; value: ani.other }
            PauseAnimation { duration: 500 }
            PropertyAction { target: state; property: "currentFace"; value: "normal" }
            PauseAnimation { duration: 500 }
            PropertyAction { target: state; property: "currentFace"; value: ani.other }
            PauseAnimation { duration: 500 }
            PropertyAction { target: state; property: "currentFace"; value: "normal" }
            PauseAnimation { duration: 500 }
        }
    }

    QQC2.Control {
        id: col
        contentItem: ColumnLayout {
            Image {
                source: `qrc:/images/taigochi/${taigochi.tData[taigochi.kind].name}/${state.currentFace}`
                Layout.alignment: Qt.AlignHCenter
            }
            Image {
                source: `qrc:/images/animations/rps/${state.currentRole}`
                Layout.alignment: Qt.AlignHCenter
            }
            RowLayout {
                QQC2.Button {
                    text: "Rock"
                    enabled: state.shuffling
                    onClicked: state.play(1)
                }
                QQC2.Button {
                    text: "Paper"
                    enabled: state.shuffling
                    onClicked: state.play(0)
                }
                QQC2.Button {
                    text: "Scissors"
                    enabled: state.shuffling
                    onClicked: state.play(2)
                }
            }
        }
    }
    
}