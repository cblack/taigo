import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.10 as QQC2
import org.kde.kirigami 2.10 as Kirigami

Window {
    id: debugWindow

    height: 500
    width: 500

    color: Kirigami.Theme.backgroundColor

    Kirigami.FormLayout {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter

        QQC2.Label { text: taigochi.tgData.hunger }
        QQC2.Label { text: taigochi.tgData.happy }
        QQC2.Label { text: taigochi.tgData.age }
        QQC2.Label { text: taigochi.tgData.weight }
        QQC2.Label { text: JSON.stringify(taigochi.tgData.kvetches) }
        QQC2.Label { text: taigochi.tgData.missedCalls }
        QQC2.Label { text: taigochi.tgData.careMisses }
        QQC2.Label { text: taigochi.tgData.stage }

        QQC2.Slider { from: 0.001; to: 1.0; value: appRoot.speedScale; onValueChanged: appRoot.speedScale = value }
    }

}
