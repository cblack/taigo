QtApplication {
    cpp.cxxLanguageVersion: "c++17"
    cpp.includePaths: ["src"]
    cpp.cppFlags: ['-Wall', '-Werror']

    files: [
        "src/*.cpp",
        "src/*.h",
    ]
    excludeFiles: [
        "src/*.license",
    ]
    Group {
        files: ["data/statemachine/*"]
        excludeFiles: ["data/statemachine/*.license"]
        fileTags: "qt.scxml.compilable"
        Qt.scxml.generateStateMethods: true
    }

    Group {
        files: ["data/qrc/**"]
        excludeFiles: ["data/qrc/**.license"]
        fileTags: "qt.core.resource_data"
        Qt.core.resourceSourceBase: "data/qrc"
        Qt.core.resourcePrefix: "/"
    }

    Depends { name: "Qt"; submodules: ["widgets"]; condition: qbs.targetOS.contains("linux") }
    Depends { name: "Qt"; submodules: ["gui", "qml", "quick", "quickcontrols2", "scxml"] }
}
